package application;

import java.io.IOException;
import java.net.UnknownHostException;

import javax.swing.JFrame;

import org.junit.jupiter.api.Test;

import Network.GameClient;
import Network.GameHost;
import javafx.application.Application;
import javafx.stage.Stage;


public class Main extends Application 
{
	int mx;
	int my;
	GameHost host;
	GameClient client;

	@Override 
	public void start(Stage stage) throws IOException 
	{    
		//setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		FlockBattle battle = new FlockBattle();
		host = battle.host;
		client = new GameClient(battle);
		System.out.println("constructed server/client");
		stage = battle;
		System.out.println("starting game loop");
		battle.gameLoop();
//GameHost host2 = new GameHost();
		stage.show();

		

	}

	public static void main(String[] args) {
		launch(args);
	}


}
