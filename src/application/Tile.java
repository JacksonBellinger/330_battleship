package application;

import java.util.Vector;

import javafx.geometry.Insets;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;


public class Tile extends Button
{
	/**
	 * 
	 */

	HitListener ship;
	HitListener socket;
	private int x;
	private int y;
	boolean hit;
	Image hitImage;
	
	public Tile(Image im, int tileSize, int x, int y)
	{
		this.x = x;
		this.y = y;
		hitImage = new Image("fire.png", tileSize , tileSize, true, true);
		ImageView fiv = new ImageView(im);
		ImageView iv = new ImageView(im);
		iv.setFitHeight(tileSize);
		iv.setFitWidth(tileSize);
		double viewX = x * tileSize;
		double viewY = y * tileSize;
		iv.setViewport(new Rectangle2D(viewX, viewY, tileSize, tileSize));
		setGraphic(iv);
		setPadding(Insets.EMPTY);
		setOnAction(value ->  {
			click("hit" + x + "," + y);
		});
		//registerListener(waterListener);
	}
	public void draw()
	{
		
	     
	     //paintComponent(dbg);
	     //dbg.drawImage(dbImage, 0, 0 , this);
	}
	public void registerListener(HitListener l) 
	//registers a listener to the tile so it can get called when there's a hit event
	{
		ship = l;
	}
	public boolean registerSocket(HitListener s)
	{
		socket = s;
		return true;
	}
	public boolean hasShip()
	{
		//return true;
		return ship != null;
	}
	public boolean hit(String msg)
	//calls all of the listeners when the tile is hit
	{
		System.out.println("tile hit " + msg);

		boolean hitReply = false;
		if(ship != null)
		{
			hitReply = ship.onHit(msg);
			socket.onHit(msg + hitReply);
		}
		//else
			//throw no ship error
		return hitReply;
	}
	
	public boolean click(String msg)
	{
		//System.out.println("click " + msg);
		if(socket != null)
			socket.click(msg);
		return true;
		//else
			//throw no socket error
	}
}
