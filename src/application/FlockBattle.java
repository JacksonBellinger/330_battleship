package application;

import java.io.IOException;
import java.net.UnknownHostException;

import Network.GameClient;
import Network.GameHost;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class FlockBattle extends Stage
{
	private BorderPane pane;
	//private MenuBar menuBar;

	private Map map;
	private Scene mapScene;
	public GameHost host;
	GameClient client;

	private boolean gameOver = false;

	private int serverPort;
	private String player2Ip;
	private int player2Port;
	//private String playerType = "Host"; //can be either Host or Client

	public Text gameType;
	public Text turnIndicator;
	boolean serverRunning = false;
	boolean playerConnected = false;
	boolean gameStarted = false;
	boolean isServer = true;
	public boolean isMyTurn = false;
	public FlockBattle() 
	{		
		host = new GameHost(this);
		client = new GameClient(this);
		gameType = new Text(0, 0, "Game type: ");
		turnIndicator = new Text(0, -10, "turn: ");
		map = new Map(10, 32, host, client);
		map.addText(gameType);
		map.addText(turnIndicator);
		//map.getChildren().add(gameType);
		//host.registerListener(map);
		pane = new BorderPane();


		mapScene = new Scene(pane, getMinHeight(), getMinWidth());

		configureMenu();

		setTitle("FlockBattle");  

		setScene(mapScene);
	}

	public void startServer()
	{
		
		new Thread(()->{
			if(!host.isStarted)
				host.start(serverPort);
			/*boolean gameover = false;
			while(!gameover) {
				System.out.println("Server turn loop");
				gameover = yourTurn();
				//myTurn();
			}*/
			serverRunning = true;
		}).start();

	}
	public void gameLoop()
	{
		boolean gameOver = false;
		new Thread(()->{
		while(!gameOver)
		{
			//System.out.println("loop");
			if(isMyTurn)
				turnIndicator.setText("It's your turn");
			else
				turnIndicator.setText("It's not your turn");
			if(!isServer) {
				if(client.recievedMsg.contains("started"))
					gameStarted = true;
				
				if(gameStarted)
				{
					//client.recievedMsg = "none";
					if(isMyTurn && client.checkForClick()) 
					{
						System.out.println(client.recievedMsg);
						isMyTurn = false;
						//client.recievedMsg = "none";
						try {
							client.sendMessage("yourturn");
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					/*if(!isMyTurn && client.recievedMsg.contains("hit"))
					{
						client.onHit();
					}*/
				}
			}
			else if(isServer)
			{
				
			}
		}
		}).start();
	}
	public void myTurn() 
	{

		System.out.println("my turn");
		if(!gameStarted)
		{
			gameStarted = true;
			while(client.checkForClick() == false)
			{
				System.out.println("my turn client waiting");
				try {
					Thread.sleep(5L * 1000L);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//wait for player to click a tile
			}
		} 


	}

	public boolean yourTurn()
	{
		/*if(isServer)
		{
			boolean waitForTurn = false;
			while(!waitForTurn)
			{
				System.out.println("your turn server waiting");

				try {
					waitForTurn = host.listen();
				} catch (IOException e) {
					e.printStackTrace();
					break;
				}
			}
			//myTurn();
		}*/
		if(!isServer)
		{
			try {
				String response = client.sendMessage("your turn");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		System.out.println("my turn done");
		return false;

	}
	public boolean validateHit(String msg)
	{
		int x = Integer.parseInt(msg.substring(3, 4));
		int y = Integer.parseInt(msg.substring(5));
		map.hit(x, y, msg);
		//isMyTurn = true;

		return true;
	}
	public boolean continueServerListen()
	{
		return !gameOver;
	}
	public void connectClient()
	{
		if(!client.started)
			try {
				client.startConnection(player2Ip, 6666);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	public void endGame()
	{

	}
	private void configureMenu()
	{


		MenuBar menuBar = new MenuBar();
		pane.setTop(menuBar);
		pane.setCenter(map);

		menuBar.prefWidthProperty().bind(widthProperty());
		menuBar.prefWidthProperty().bind(widthProperty());

		// File menu - new, save, exit
		Menu fileMenu = new Menu("File");
		MenuItem clientMenuItem = new MenuItem("Connect to Player");
		MenuItem serverMenuItem = new MenuItem("Start Server");

		clientMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				BorderPane root = new BorderPane();
				Stage stage = new Stage();
				stage.setTitle("Connect to a Player");

				Label ipLabel = new Label("Enter Player's IP Address:");

				TextField ipTextField = new TextField ();
				ipTextField.setPromptText("Player 2 IP Address:");
				ipTextField.setPrefWidth(20);
				TextField portTextField = new TextField ();
				portTextField.setPromptText("Player 2 port:");
				portTextField.setPrefWidth(20);

				Button ipConnectButton = new Button("Start Connection");
				ipConnectButton.setOnAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent actionEvent) {
						player2Ip = "127.0.0.1";//ipTextField.getText();
						player2Port = 6666;// Integer.parseInt(portTextField.getText());
						System.out.println(player2Port);
						isServer = false;
						gameType.setText("client");
						isMyTurn = true;
						try {
							client.startConnection(player2Ip, player2Port);
							playerConnected = client.started;
							map.registerSockets();
						} catch (UnknownHostException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						}
						gameLoop();

						//						new Thread( ()->{
						//							try {
						//								playerType = "client";
						//								System.out.println("starting client");
						//								client.startConnection("127.0.0.1", 6666);
						//								String response = client.sendMessage("start " + ipTextField.getText());
						//								System.out.println("C: greet response " + response);
						//								clientTest(client);
						//							} catch (IOException e) {
						//								e.printStackTrace();
						//							}
						//						}).start();
					}
				});

				VBox vb = new VBox(8);
				vb.getChildren().addAll(ipLabel, ipTextField, portTextField, ipConnectButton);
				vb.setPrefWidth(100);

				root.setCenter(vb);
				stage.setScene(new Scene(root, 450, 450));
				stage.show();
			}
		});

		serverMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				BorderPane root = new BorderPane();
				Stage stage = new Stage();
				stage.setTitle("Start a GameHost");

				TextField portTextField = new TextField ();
				portTextField.setPromptText("Player 2 port:");
				portTextField.setPrefWidth(20);

				Button serverStartButton = new Button("Start Connection");
				serverStartButton.setOnAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent actionEvent) {
						serverPort = 6666;//integer.parseInt(portTextField.getText());
						System.out.println(serverPort);
						isServer = true;
						gameType.setText("server");
						startServer();

					}
				});

				VBox vb = new VBox(8);
				vb.getChildren().addAll(portTextField, serverStartButton);
				vb.setPrefWidth(100);

				root.setCenter(vb);
				stage.setScene(new Scene(root, 450, 450));
				stage.show();
			}
		});

		MenuItem exitMenuItem = new MenuItem("Exit");
		exitMenuItem.setOnAction(actionEvent -> Platform.exit());

		fileMenu.getItems().addAll(clientMenuItem, serverMenuItem, new SeparatorMenuItem(), exitMenuItem);

		menuBar.getMenus().addAll(fileMenu);
	}
}
