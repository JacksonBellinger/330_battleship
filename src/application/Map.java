package application;

import Network.GameClient;
import Network.GameHost;
import food.AbstractFoodsImpl;
import food.Fries;
import food.HotDog;
import food.IceCream;
import food.Popcorn;
import food.Shake;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

public class Map extends Group{

	private GridPane playerGrid;
	private GridPane enemyGrid;
	private Tile[][] playerTiles;
	private Tile[][] enemyTiles;
	
	private AbstractFoodsImpl[] foods;
	private int size;
	private static Image waterImage;
	private static Image transWaterImage;
	
	private GameHost host;
	private GameClient client;
	FlockBattle game;
	Text[] textFields;

	//might need references to the player objects

	public Map(int numTiles, int tileSize, GameHost h, GameClient c)
	{
		//construct the player and enemy map gridpanes and add them to the map node group
		host = h;
		client = c;
		game = h.game;

		//host.registerListener(this);
		playerGrid = new GridPane();
		enemyGrid = new GridPane();
		
		playerGrid.setPrefWidth(32);
		playerGrid.setMaxWidth(32);
		playerGrid.setPrefHeight(32);
		playerGrid.setMaxHeight(32);

		
		enemyGrid.setPrefWidth(32);
		enemyGrid.setMaxWidth(32);
		enemyGrid.setPrefHeight(32);
		enemyGrid.setMaxHeight(32);


		getChildren().add(playerGrid);
		getChildren().add(enemyGrid);
		
		
		size = numTiles * tileSize;
		playerGrid.setTranslateY(size + tileSize/2);

		playerTiles = new Tile[numTiles][numTiles];
		enemyTiles = new Tile[numTiles][numTiles];
		int imageSize = tileSize * numTiles;
		waterImage = new Image("beach.jpeg", imageSize, imageSize, true, true);
		Image beachImage = new Image("beach.jpeg", imageSize, imageSize, true, true);
		for(int i = 0; i < numTiles; i++)
			for(int j = 0; j < numTiles; j++)
			{
				playerTiles[i][j] = new Tile(beachImage, tileSize, i, j);
				enemyTiles[i][j] = new Tile(transWaterImage, tileSize, i, j);
				//playerTiles[i][j].registerSocket(host);
				
				enemyGrid.add(enemyTiles[i][j], i, j, 1, 1);
				playerGrid.add(playerTiles[i][j], i, j, 1, 1);
			}
		registerSockets();
		foods = new AbstractFoodsImpl[5];
		foods[0] = new Fries();
		foods[1] = new IceCream();
		foods[2] = new HotDog();
		foods[3] = new Popcorn();
		foods[4] = new Shake();
		
	}
	public void registerSockets()
	{
		for(Tile[] tt : enemyTiles)
			for(Tile t : tt)//for each tile
			{
				if(game.isServer)
					t.registerSocket(host);
				else
					t.registerSocket(client);
			}
	}
	public void addText(Text t)
	{
		getChildren().add(t);
	}

	public boolean hasShip(int x, int y)
	{
		return playerTiles[x][y].hasShip();
	}
	public boolean hit(int x, int y, String msg)
	{
		return playerTiles[x][y].hit(msg);
	}
	
	public void register(int x, int y, HitListener hl)
	{
		playerTiles[x][y].registerListener(hl);
	}

	public void hitEnemyTile(int x, int y, String msg)
	{
		enemyTiles[x][y].hit(msg);
	}

}
