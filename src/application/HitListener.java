package application;


public interface HitListener 
{
	boolean onHit(String msg);
	boolean click(String msg);
}
