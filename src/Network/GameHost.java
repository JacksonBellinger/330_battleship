package Network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Vector;
import java.util.concurrent.TimeUnit;

import application.FlockBattle;
import application.HitListener;

public class GameHost implements HitListener{
	private ServerSocket serverSocket;
	private Socket clientSocket;
	private PrintWriter out;
	private BufferedReader in;
	private Vector<HitListener> listeners;
	public FlockBattle game;
	public boolean isStarted = false;
	public String clicked = "none";
	String recievedMsg = "none";
	Runnable serverListener;
	Thread thread;

	public GameHost(FlockBattle g)
	{
		game = g;
	}
	public void start(int port) {
		isStarted = true;
		listeners = new Vector<HitListener>();
		//game = g;
		System.out.println("S: Starting server on port: " + port);
		try {
			serverSocket = new ServerSocket(port);

			clientSocket = serverSocket.accept();
			out = new PrintWriter(clientSocket.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			serverListener = new Runnable(){
				public void run(){
					while(game.continueServerListen()) 
					{
						System.out.println("running");
						try {
							listen();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			};

			thread = new Thread(serverListener);
			thread.start();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public boolean listen() throws IOException
	{
		boolean gotMessage = false;
		System.out.println("listening : in readline");
		recievedMsg = in.readLine();
		if(recievedMsg != null)
		{
			System.out.println("message: " + recievedMsg);
			if (recievedMsg.contains("start")) {
				System.out.println("S: server started");
				out.println("S: started");
				return true;
			}
			else if (recievedMsg.contains("end")) {
				out.println("good bye");
				game.endGame();
				return true;
			}
			else if(recievedMsg.contains("hit"))
			{
				String temp = "" + onHit(recievedMsg);
				System.out.println(temp);
				out.println("" + onHit(recievedMsg) );
				try {
					TimeUnit.SECONDS.sleep(2);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//game.myTurn();
				return true;
			}
			else if(recievedMsg.contains("turn"))
			{
				System.out.println("Server's turn");
				game.isMyTurn = true;
				while(clicked.contains("none"))
				{
					try {
						Thread.sleep(3);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//System.out.println(clicked);
					//return false;

					
				}
				
				if(clicked.contains("hit"))
				{
					System.out.println("S: sending hit reply");
					out.println(clicked);
					return true;
				}
			}
			else {
				out.println("S: unrecognised message");
			}
			//out.flush();
		}

		return false;
	}
	public void stop() throws IOException {
		System.out.println("closing server");
		in.close();
		out.close();
		clientSocket.close();
		serverSocket.close();
		isStarted = false;
	}

	public boolean registerListener(HitListener l) 
	//registers a listener to the host so it can get called when there's a hit event
	{
		return listeners.add(l);
	}


	@Override
	public boolean onHit(String msg) {
		System.out.println("S: recieved a hit " + msg);
		if(game.isMyTurn) 
			System.out.println("can't get hit on my turn");

		else if(game.validateHit(msg))
			return true;
		return false;
	}
	@Override
	public boolean click(String msg) {
		if(!game.isMyTurn)
			return false;
		if(msg.contains("hit"))
		{
			clicked = msg;
			return true;
		}
		return false;

	}
}
