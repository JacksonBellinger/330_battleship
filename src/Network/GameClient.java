package Network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;

import application.FlockBattle;
import application.HitListener;

public class GameClient implements HitListener{
	public boolean started = false;
	private Socket clientSocket;
	private PrintWriter out;
	private BufferedReader in;
	
	private boolean sentMessage = false;
	public String recievedMsg = "none";

	FlockBattle game;
	
	public GameClient(FlockBattle g)
	{
		game = g;
	}
	public void startConnection(String ip, int port) throws UnknownHostException, IOException {
		System.out.println("starting connection");
		clientSocket = new Socket(ip, port);
		out = new PrintWriter(clientSocket.getOutputStream(), true);
		in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
		recievedMsg = sendMessage("C: start");
		started = recievedMsg.contains("started");
	}

	public String sendMessage(String msg) throws IOException {
		//String resp = "not sent";
		if(sentMessage)
			System.out.println("Tried to send 2 messages, please wait");
		else {
			System.out.println("C: sending msg " + msg);
			out.println(msg);
			//sentMessage = true;
			recievedMsg = in.readLine();
			System.out.println("C: msg response " + recievedMsg);
			if(recievedMsg.contains("hit"))
			{
				onHit(recievedMsg);
				game.isMyTurn = true;
			}
			//sentMessage = false;
			out.flush();
		}
		return recievedMsg;
	}

	public void stopConnection() throws IOException {
		in.close();
		out.close();
		clientSocket.close();
	}
	
	public boolean checkForClick()
	{
		if(sentMessage)
		{
			sentMessage = false;
			return true;
		}
		else
			return false;
	}
	@Override
	public boolean onHit(String msg) {
		System.out.println("C: recieved a hit " + msg);
		if(game.isMyTurn) 
			System.out.println("can't get hit on my turn");
		
		else if(game.validateHit(msg))
			return true;
		return false;
	}
	@Override
	public boolean click(String msg) {
		System.out.println("click " + msg);
		if(!game.isMyTurn)
			return false;
		System.out.println("C: click " + msg);
		try {
			if((recievedMsg = sendMessage(msg)) != null)
			{
				System.out.println(recievedMsg);
				sentMessage = true;
				if(recievedMsg.contains("endgame"))
					game.endGame();
				else
				{
					try {
						TimeUnit.SECONDS.sleep(2);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//game.yourTurn();
				}
				return true;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
}
