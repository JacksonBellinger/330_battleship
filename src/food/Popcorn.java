package food;
import java.io.File;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

public class Popcorn extends AbstractFoodsImpl
{
    public String path;
    String die = "Death.mp3";
    String musicFile = "Squawk.mp3";
    
    
    public Popcorn (){
        hp = 4;
        y = 0;
        x = 0;
        path = "https://www.twosisterscrafting.com/wp-content/uploads/2016/01/perfect-popcorn-main.jpg";
    }
    public void reduceHealth()
    {
        if (hp != 0)
        {
            --hp;
        Media sound = new Media(new File(musicFile).toURI().toString());
        MediaPlayer mediaPlayer = new MediaPlayer(sound);
        mediaPlayer.play();
        }
        if (hp == 0)
        {
        Media sound = new Media(new File(die).toURI().toString());
        MediaPlayer mediaPlayer = new MediaPlayer(sound);
        mediaPlayer.play();       
        }
    }
}