package food;
import java.io.File;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

public class Shake extends AbstractFoodsImpl
{
    
    public String path;
    String die = "Death.mp3";
    String musicFile = "Squawk.mp3";
    
    public Shake ()
    {
        hp = 3;
        y = 0;
        x = 0;
        path = "https://www.browneyedbaker.com/wp-content/uploads/2011/03/shamrock-shake-3-800.jpg";
    }
    public void reduceHealth()
    {
        if (hp != 0)
        {
            --hp;
        Media sound = new Media(new File(musicFile).toURI().toString());
        MediaPlayer mediaPlayer = new MediaPlayer(sound);
        mediaPlayer.play();
        }
        if (hp == 0)
        {
        Media sound = new Media(new File(die).toURI().toString());
        MediaPlayer mediaPlayer = new MediaPlayer(sound);
        mediaPlayer.play();       
        }
    }     
}