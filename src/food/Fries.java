package food;
import java.io.File;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

public class Fries extends AbstractFoodsImpl
{
    public String path;
    String die = "Death.mp3";
    String musicFile = "Squawk.mp3";
    
    public Fries (){
        hp = 2;
        y = 0;
        x = 0;
        path = "https://previews.123rf.com/images/anatchant/anatchant1508/anatchant150800023/43967994-potatoes-fries-isolated-on-white.jpg";
    }
    
    public void reduceHealth()
    {
        if (hp != 0)
        {
            --hp;
        Media sound = new Media(new File(musicFile).toURI().toString());
        MediaPlayer mediaPlayer = new MediaPlayer(sound);
        mediaPlayer.play();
        }
        if (hp == 0)
        {
        Media sound1 = new Media(new File(die).toURI().toString());
        MediaPlayer media = new MediaPlayer(sound1);
        media.play();       
        }
    }
}